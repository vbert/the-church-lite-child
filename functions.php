<?php
/**
 * The Church Lite Child Theme
 * 
 * @author vbert <wsobczak@gmail.com>
 * @since 1.0.0
 */

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

// for problem with css, use this:
/*
function my_theme_enqueue_styles() {
 
    $parent_style = 'jobcareertheme';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('1.0.0')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
*/
