# README #

- *Theme Name:* The Church Lite Child
- *Theme URI:* https://gracethemes.com/themes/free-church-religious-wordpress-theme
- *Description:* The Church Lite Child Theme
- *Author:* vbert
- *Author URI:* https://bitbucket.org/vbert/
- *Template:* thechurchlite
- *Version:* 1.0.0
- *License:* GNU General Public License v2 or later
- *License URI:* http://www.gnu.org/licenses/gpl-2.0.html
- *Tags:* blog,two-columns,right-sidebar,full-width-template,custom-colors,custom-menu,custom-header,custom-logo,featured-images,editor-style,custom-background,threaded-comments,theme-options, translation-ready
- *Text Domain:* thechurchlitechild
